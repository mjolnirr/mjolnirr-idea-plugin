package com.mjolnirr.browser;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 12.11.13
 * Time: 12:49
 * To change this template use File | Settings | File Templates.
 */
public class BrowserRunner {
    /**
     * Randomly chosen, but static, high socket number
     */
    public static final int SINGLE_INSTANCE_NETWORK_SOCKET = 45893;

    /**
     * Must end with newline
     */
    public static final String SINGLE_INSTANCE_SHARED_KEY = "$$NewInstance$$\n";

    public static void run(String url) throws IOException {
        Socket clientSocket = new Socket(InetAddress.getByAddress(new byte[]{127, 0, 0, 1}), SINGLE_INSTANCE_NETWORK_SOCKET);
        OutputStream out = clientSocket.getOutputStream();
        out.write(SINGLE_INSTANCE_SHARED_KEY.getBytes());
        out.write(("[\"" + url + "\"]\n").getBytes());
        out.close();
        clientSocket.close();
    }
}
