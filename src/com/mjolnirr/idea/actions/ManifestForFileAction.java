package com.mjolnirr.idea.actions;

import com.intellij.ide.SelectInTarget;
import com.intellij.ide.projectView.ProjectView;
import com.intellij.ide.projectView.impl.ProjectViewTree;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DataKeys;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.MessageType;
import com.intellij.openapi.ui.popup.Balloon;
import com.intellij.openapi.ui.popup.JBPopupFactory;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.wm.StatusBar;
import com.intellij.openapi.wm.WindowManager;
import com.intellij.psi.PsiClass;
import com.intellij.ui.awt.RelativePoint;
import com.mjolnirr.idea.manifest.Generator;

import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 04.11.13
 * Time: 16:30
 * To change this template use File | Settings | File Templates.
 */
public class ManifestForFileAction extends MjolnirrAction {
    public void actionPerformed(AnActionEvent e) {
        initialize(e);

        Project currentProject = DataKeys.PROJECT.getData(e.getDataContext());

        Object[] selected = ProjectView.getInstance(currentProject).getCurrentProjectViewPane().getSelectedElements();

        if (selected.length != 1) {
            //  Too many or too few items selected
            //  Notify
            showError("Too many items!", "You must select only one class!");

            return;
        }

        if (!(selected[0] instanceof PsiClass)) {
            showError("Incorrect object selected", "Please select class");

            return;
        }

        PsiClass classToGenerate = (PsiClass) selected[0];
        Generator manifestGenerator = new Generator(currentProject);

        if (!manifestGenerator.isMjolnirrComponent(classToGenerate)) {
            showError("Class is not Mjolnirr component!", "Class must extend either AbstractApplication or AbstractModule");

            return;
        }

        manifestGenerator.writeManifest(classToGenerate);
    }
}
