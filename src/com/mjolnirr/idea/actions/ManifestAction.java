package com.mjolnirr.idea.actions;

import com.intellij.ide.util.TreeClassChooser;
import com.intellij.ide.util.TreeClassChooserFactory;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DataKeys;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ModuleRootManager;
import com.intellij.openapi.roots.ProjectRootManager;
import com.intellij.openapi.vfs.*;
import com.intellij.psi.*;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.util.PsiUtil;
import com.mjolnirr.idea.manifest.*;
import com.mjolnirr.idea.manifest.description.Envelope;
import com.mjolnirr.idea.manifest.description.Manifest;
import com.mjolnirr.idea.manifest.description.ManifestMethod;
import com.mjolnirr.idea.manifest.description.ManifestParameter;
import com.thoughtworks.xstream.XStream;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 31.10.13
 * Time: 16:06
 * To change this template use File | Settings | File Templates.
 */
public class ManifestAction extends MjolnirrAction {
    private Project currentProject;
    private PsiClass abstractComponentClass;

    public void actionPerformed(AnActionEvent e) {
        currentProject = DataKeys.PROJECT.getData(e.getDataContext());

        abstractComponentClass = JavaPsiFacade.getInstance(currentProject).findClass("com.mjolnirr.lib.component.AbstractComponent", GlobalSearchScope.allScope(currentProject));

        TreeClassChooser result = TreeClassChooserFactory
                .getInstance(currentProject)
                .createInheritanceClassChooser("Choose the class to generate manifest",
                        GlobalSearchScope.projectScope(currentProject),
                        abstractComponentClass,
                        false,
                        false,
                        null);
        result.showDialog();

        PsiClass classToGenerate = result.getSelected();

        Generator manifestGenerator = new Generator(currentProject);

        manifestGenerator.writeManifest(classToGenerate);
    }
}
