package com.mjolnirr.idea.actions;

import com.intellij.ide.projectView.ProjectView;
import com.intellij.ide.util.TreeClassChooser;
import com.intellij.ide.util.TreeClassChooserFactory;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DataKeys;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.JavaPsiFacade;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiManager;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.util.ClassUtil;
import com.intellij.psi.util.PsiClassUtil;
import com.mjolnirr.idea.manifest.Generator;
import com.mjolnirr.idea.manifest.Util;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 31.10.13
 * Time: 16:06
 * To change this template use File | Settings | File Templates.
 */
public class ManifestForCurrentAction extends MjolnirrAction {
    private Project currentProject;
    private PsiClass abstractComponentClass;

    public void actionPerformed(AnActionEvent e) {
        initialize(e);

        currentProject = DataKeys.PROJECT.getData(e.getDataContext());
        VirtualFile[] selectedFiles = FileEditorManager.getInstance(currentProject).getSelectedFiles();

        for (VirtualFile f : selectedFiles) {
            if (f.getFileType().getName().equalsIgnoreCase("JAVA")) {
                PsiClass classToGenerate = Util.getClassNameFromFile(f, currentProject);
                Generator manifestGenerator = new Generator(currentProject);

                if (manifestGenerator.isMjolnirrComponent(classToGenerate)) {
                    manifestGenerator.writeManifest(classToGenerate);

                    return;
                }
            }
        }

        showError("No Mjolnirr components opened", "Manifest can be generated for Mjolnirr components only");
    }
}
