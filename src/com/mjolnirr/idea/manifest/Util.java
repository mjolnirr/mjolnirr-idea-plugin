package com.mjolnirr.idea.manifest;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ModuleRootManager;
import com.intellij.openapi.roots.ProjectRootManager;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.*;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.search.searches.ClassInheritorsSearch;
import com.intellij.util.Query;

import java.io.IOException;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 04.11.13
 * Time: 15:27
 * To change this template use File | Settings | File Templates.
 */
public class Util {
    public static boolean isPrimitive(PsiTypeElement param) {
        //  void is void
        if (param.getType().getCanonicalText().equals("void")) { return false; }

        return param.getType().getClass().getCanonicalName().equals("com.intellij.psi.PsiPrimitiveType");
    }

    public static PsiTypeElement handlePrimitive(PsiTypeElement param, Project currentProject) {
        PsiPrimitiveType primitive = (PsiPrimitiveType) param.getType();

        PsiElementFactory typeFactory = PsiElementFactory.SERVICE.getInstance(currentProject);

        param.replace(typeFactory.createTypeElement(typeFactory.createTypeByFQClassName(primitive.getBoxedTypeName())));

        return param;
    }

    public static PsiClass getClassNameFromFile(VirtualFile file, Project currentProject) {
        Module module = ProjectRootManager.getInstance(currentProject).getFileIndex().getModuleForFile(file);
        ModuleRootManager rootManager = ModuleRootManager.getInstance(module);

        VirtualFile[] resourceRoots = rootManager.getSourceRoots();

        for (VirtualFile resource : resourceRoots) {
            if (file.getCanonicalPath().startsWith(resource.getCanonicalPath())) {
                String className = file.getCanonicalPath()
                        .replace(resource.getCanonicalPath(), "")
                        .replace(".java", "")
                        .substring(1)
                        .replace("/", ".");

                return JavaPsiFacade.getInstance(currentProject).findClass(className, GlobalSearchScope.projectScope(currentProject));
            }
        }

        return null;
    }

    public static List<VirtualFile> getMjolnirrModules(Project project) {
        PsiClass abstractComponentClass = JavaPsiFacade.getInstance(project).findClass("com.mjolnirr.lib.component.AbstractComponent", GlobalSearchScope.allScope(project));
        Collection<PsiClass> classes = ClassInheritorsSearch.search(abstractComponentClass, GlobalSearchScope.projectScope(project), true).findAll();

        List<VirtualFile> resultJARs = new ArrayList<VirtualFile>();

        for (PsiClass component : classes) {
            Module compModule = ProjectRootManager.getInstance(project).getFileIndex().getModuleForFile(component.getContainingFile().getVirtualFile());
            VirtualFile[] contentRoots = ModuleRootManager.getInstance(compModule).getContentRoots();

            for (VirtualFile f : contentRoots) {
                List<VirtualFile> jars = new ArrayList<VirtualFile>();
                List<VirtualFile> targetContent = new ArrayList<VirtualFile>(Arrays.asList(f.findChild("target").getChildren()));
                for (VirtualFile targetChild : targetContent) {
                    if (targetChild.isDirectory()) {
                        continue;
                    }

                    if (targetChild.getExtension().toLowerCase().contains("jar")) {
                        jars.add(targetChild);
                    }
                }
                VirtualFile[] targetContentArray = jars.toArray(new VirtualFile[]{});
                Arrays.sort(targetContentArray, new Comparator<VirtualFile>() {
                    @Override
                    public int compare(VirtualFile o1, VirtualFile o2) {
                        return (int) Math.signum(o2.getLength() - o1.getLength());
                    }
                });

                resultJARs.add(targetContentArray[0]);
            }
        }

        return resultJARs;
    }

    public static void deployComponents(final Project project) throws IOException {
        ApplicationManager.getApplication().runWriteAction(new Runnable() {
            @Override
            public void run() {
                try {
                    VirtualFile oldDeployDirectory = project.getBaseDir().findChild(".mjolnirr");
                    if (oldDeployDirectory != null) { oldDeployDirectory.delete(project); }

                    VirtualFile deployDirectory = project.getBaseDir().createChildDirectory(project, ".mjolnirr");

                    List<VirtualFile> components = getMjolnirrModules(project);

                    for (VirtualFile jar : components) {
                        jar.copy(project, deployDirectory, jar.getName());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static String getFirstMjolnirrComponent(Project project, VirtualFile selectedModule) {
        Module module = ProjectRootManager.getInstance(project).getFileIndex().getModuleForFile(selectedModule);

        PsiClass abstractComponentClass = JavaPsiFacade.getInstance(project).findClass("com.mjolnirr.lib.component.AbstractComponent", GlobalSearchScope.allScope(project));
        PsiClass firstMjolnirrComponent = ClassInheritorsSearch.search(abstractComponentClass, GlobalSearchScope.moduleScope(module), true).findFirst();

        return firstMjolnirrComponent.getName();
    }
}
