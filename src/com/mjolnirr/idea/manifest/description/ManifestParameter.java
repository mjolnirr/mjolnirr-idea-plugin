package com.mjolnirr.idea.manifest.description;

import com.intellij.psi.PsiTypeElement;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 29.10.13
 * Time: 12:19
 * To change this template use File | Settings | File Templates.
 */
@XStreamAlias("parameter")
public class ManifestParameter {
    @XStreamAsAttribute
    private String type;

    public ManifestParameter(PsiTypeElement type) {
        this.type = type.getType().getCanonicalText().replaceAll("\\<.*?\\>", "");
    }

    @Override
    public int hashCode() {
        return type.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof ManifestParameter)) {
            return false;
        }

        ManifestParameter other = (ManifestParameter) obj;

        return type.equals(other.type);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}