package com.mjolnirr.idea.manifest.description;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 29.10.13
 * Time: 12:18
 * To change this template use File | Settings | File Templates.
 */
@XStreamAlias("method")
public class ManifestMethod {
    @XStreamAsAttribute
    private String name;

    @XStreamAsAttribute
    @XStreamAlias("type")
    private String returnType;

    private List<ManifestParameter> parameters;

    public ManifestMethod(String name, String returnType, List<ManifestParameter> parameters) {
        this.name = name.replaceAll("\\<.*?\\>", "");
        this.returnType = returnType;
        this.parameters = parameters;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof ManifestMethod)) {
            return false;
        }

        ManifestMethod other = (ManifestMethod) obj;

        if (!name.equals(other.name)) { return false; }
        if (!returnType.equals(other.returnType)) { return false; }
        if (parameters.size() != other.parameters.size()) { return false; }

        for (int i = 0 ; i < parameters.size() ; i++) {
            if (!parameters.get(i).equals(other.parameters.get(i))) { return false; }
        }

        return true;
    }

    @Override
    public int hashCode() {
        return name.hashCode() + returnType.hashCode() + parameters.hashCode();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReturnType() {
        return returnType;
    }

    public void setReturnType(String returnType) {
        this.returnType = returnType;
    }

    public List<ManifestParameter> getParameters() {
        return parameters;
    }

    public void setParameters(List<ManifestParameter> parameters) {
        this.parameters = parameters;
    }
}