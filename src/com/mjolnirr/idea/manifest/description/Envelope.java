package com.mjolnirr.idea.manifest.description;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 29.10.13
 * Time: 11:29
 * To change this template use File | Settings | File Templates.
 */
@XStreamAlias("hive")
public class Envelope {
    private Manifest manifest;

    public Envelope(Manifest manifest) {
        this.manifest = manifest;
    }

    public Manifest getManifestBody() {
        return manifest;
    }
}