package com.mjolnirr.idea.manifest;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ModuleRootManager;
import com.intellij.openapi.roots.ProjectRootManager;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.*;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.util.PsiUtil;
import com.mjolnirr.idea.manifest.description.Envelope;
import com.mjolnirr.idea.manifest.description.Manifest;
import com.mjolnirr.idea.manifest.description.ManifestMethod;
import com.mjolnirr.idea.manifest.description.ManifestParameter;
import com.thoughtworks.xstream.XStream;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 05.11.13
 * Time: 12:40
 * To change this template use File | Settings | File Templates.
 */
public class Generator {
    private Project currentProject;
    private PsiClass abstractApplicationClass;
    private PsiClass abstractModuleClass;
    private PsiClass abstractComponentClass;

    public Generator(Project project) {
        currentProject = project;

        abstractComponentClass = JavaPsiFacade.getInstance(currentProject).findClass("com.mjolnirr.lib.component.AbstractComponent", GlobalSearchScope.allScope(currentProject));
        abstractApplicationClass = JavaPsiFacade.getInstance(currentProject).findClass("com.mjolnirr.lib.component.AbstractApplication", GlobalSearchScope.allScope(currentProject));
        abstractModuleClass = JavaPsiFacade.getInstance(currentProject).findClass("com.mjolnirr.lib.component.AbstractModule", GlobalSearchScope.allScope(currentProject));
    }


    public boolean isMjolnirrComponent(PsiClass classToGenerate) {
        return (classToGenerate.isInheritorDeep(abstractComponentClass, abstractComponentClass));
    }

    public void writeManifest(final PsiClass classToGenerate) {
        Module module = ProjectRootManager.getInstance(currentProject).getFileIndex().getModuleForFile(classToGenerate.getContainingFile().getVirtualFile());
        ModuleRootManager rootManager = ModuleRootManager.getInstance(module);

        VirtualFile[] resourceRoots = rootManager.getSourceRoots();

        for (final VirtualFile resources : resourceRoots) {
            if (resources.getName().toLowerCase().startsWith("res")) {
                //  This is it
                ApplicationManager.getApplication().runWriteAction(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            VirtualFile manifestFile = resources.createChildData(resources, "manifest.xml");

                            manifestFile.setBinaryContent(serializeManifest(parseClass(classToGenerate)).getBytes());
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                    }
                });
            }
        }
    }

    private String serializeManifest(Envelope manifest) {
        final XStream xstream = new XStream();

        xstream.aliasField(manifest.getManifestBody().getType(), Envelope.class, "manifest");
        xstream.processAnnotations(Envelope.class);
        xstream.processAnnotations(Manifest.class);
        xstream.processAnnotations(ManifestMethod.class);
        xstream.processAnnotations(ManifestParameter.class);

        return xstream.toXML(manifest);
    }

    private Envelope parseClass(PsiClass classToGenerate) {
        List<ManifestMethod> methods = new ArrayList<ManifestMethod>();

        for (PsiMethod method : classToGenerate.getAllMethods()) {
            //  If this method is inherited from the Object class we don't need it
            if (methodNotNeeded(method)) {
                continue;
            }

            List<ManifestParameter> parameters = new ArrayList<ManifestParameter>();

            for (PsiParameter param : method.getParameterList().getParameters()) {
                PsiTypeElement type = handleType(param.getTypeElement());

                parameters.add(new ManifestParameter(type));
            }

            if (method.getReturnType() != null) {
                PsiTypeElement returnType = handleType(method.getReturnTypeElement());

                ManifestMethod manifestMethod = new ManifestMethod(method.getName(),
                        returnType.getType().getCanonicalText(),
                        parameters);

                if (!methods.contains(manifestMethod) && isPublic(method)) {
                    methods.add(manifestMethod);
                }
            }
        }

        int componentType = Manifest.TYPE_APPLICATION;
        if (classToGenerate.isInheritorDeep(abstractModuleClass, abstractComponentClass)) {
            componentType = Manifest.TYPE_MODULE;
        }

        Manifest manifest = new Manifest(componentType, classToGenerate.getName(), classToGenerate.getQualifiedName(), methods);

        return new Envelope(manifest);
    }

    private PsiTypeElement handleType(PsiTypeElement type) {
        if (Util.isPrimitive(type)) {
            type = Util.handlePrimitive(type, currentProject);
        }

        return type;
    }

    private boolean isPublic(PsiMethod method) {
        return (PsiUtil.getAccessLevel(method.getModifierList()) == PsiUtil.ACCESS_LEVEL_PUBLIC);
    }

    private boolean methodNotNeeded(PsiMethod method) {
        if (method.getContainingClass().getQualifiedName().equals("java.lang.Object")) { return true; }
        if (method.getContainingClass().getQualifiedName().equals("com.mjolnirr.lib.component.AbstractComponent")) { return true; }
        if (method.getContainingClass().getQualifiedName().equals("com.mjolnirr.lib.component.AbstractApplication")) { return true; }
        if (method.getContainingClass().getQualifiedName().equals("com.mjolnirr.lib.component.AbstractModule")) { return true; }
        if (method.isConstructor()) { return true; }

        if (!method.getName().equals("initialize")) {
            return false;
        }

        PsiParameter[] methodParameters = method.getParameterList().getParameters();

        if (methodParameters.length != 1) {
            return false;
        }

        return methodParameters[0].getType().getCanonicalText().equals("com.mjolnirr.lib.component.ComponentContext");
    }
}
