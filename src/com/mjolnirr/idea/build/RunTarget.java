package com.mjolnirr.idea.build;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 06.11.13
 * Time: 11:29
 * To change this template use File | Settings | File Templates.
 */
public class RunTarget {
//    EMULATOR("tcp"),
//    DEVICE("usb");

    private String id;

    RunTarget(String id) {
        this.id = id;
    }

    @Override
    public final String toString() {
//        switch (this) {
//            case EMULATOR:
//                return "Emulator";
//            case DEVICE:
//                return "Device";
//        }
        return "Undefined";
    }

    /**
     * Returns the id of this target as used in the palm command line tools. For the emulator the id is 'tcp', for a real device it is
     * 'usb'.
     */
    public final String getId() {
        return id;
    }
}
