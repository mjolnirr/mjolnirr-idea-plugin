package com.mjolnirr.idea.build;

import com.intellij.execution.configurations.ConfigurationType;
import com.intellij.execution.configurations.ConfigurationTypeBase;
import com.mjolnirr.idea.MjolnirrApplicationComponent;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 05.11.13
 * Time: 18:46
 * To change this template use File | Settings | File Templates.
 */
public class MjolnirrRunConfigurationType extends ConfigurationTypeBase implements ConfigurationType {
    public MjolnirrRunConfigurationType() {
        super("Mjolnirr.RunConfigurationType", "Mjolnirr component", "Mjolnirr component", MjolnirrApplicationComponent.ICON);
        addFactory(new MjolnirrConfigFactory(this));
    }

    @NonNls
    @NotNull
    public String getComponentName() {
        return "WebOS.RunConfigurationType";
    }
}
