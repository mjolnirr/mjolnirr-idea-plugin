package com.mjolnirr.idea.build;

import com.intellij.execution.ExecutionException;
import com.intellij.execution.ExecutionManager;
import com.intellij.execution.configurations.GeneralCommandLine;
import com.intellij.execution.process.OSProcessHandler;
import com.intellij.execution.process.ProcessHandler;
import com.intellij.ide.plugins.IdeaPluginDescriptor;
import com.intellij.ide.plugins.PluginManager;
import com.intellij.openapi.extensions.PluginId;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.MessageType;
import com.intellij.openapi.ui.popup.Balloon;
import com.intellij.openapi.ui.popup.JBPopupFactory;
import com.intellij.openapi.util.Key;
import com.intellij.openapi.wm.StatusBar;
import com.intellij.openapi.wm.WindowManager;
import com.intellij.ui.awt.RelativePoint;
import com.mjolnirr.browser.BrowserRunner;
import com.mjolnirr.idea.MjolnirrApplicationComponent;
import com.mjolnirr.idea.manifest.Util;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 06.11.13
 * Time: 11:11
 * To change this template use File | Settings | File Templates.
 */
public class MjolnirrRunner {
    public static final Key<MjolnirrRunConfiguration> MJOLNIRR_RUN_CONFIGURATION_KEY = Key.create("MjolnirrRunConfiguration");
    private static final int BROWSER_RUNNING_TIMEOUT = 4;

    private boolean appAlreadyRunning;
    private OSProcessHandler processHandler;

    public MjolnirrRunner(Project project, MjolnirrRunConfiguration config, BuildMode buildMode) throws ExecutionException {
        try {
            Util.deployComponents(project);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ProcessHandler[] runningProcesses = ExecutionManager.getInstance(project).getRunningProcesses();
        for (ProcessHandler process : runningProcesses) {
            if (!process.isProcessTerminated()) {
                MjolnirrRunConfiguration data = process.getUserData(MJOLNIRR_RUN_CONFIGURATION_KEY);
                if (data != null) {
                    if (data.equals(config)) {
                        appAlreadyRunning = true;
                    }
                }
            }
        }

//        IdeaPluginDescriptor descriptor = PluginManager.getPlugin(PluginId.getId(MjolnirrApplicationComponent.PLUGIN_ID));
        File containerRunJar = new File(config.getContainerJARLocation());

        GeneralCommandLine command = new GeneralCommandLine();
        command.setExePath("java");
        command.addParameter("-jar");
        command.addParameter(containerRunJar.getAbsolutePath());
        command.addParameter("--host");
        command.addParameter("localhost");
        command.addParameter("--port");
        command.addParameter("" + config.getContainerPort());
        command.addParameter("--mode");
        command.addParameter("dev");
        command.addParameter("--jar");
        command.addParameter(project.getBasePath() + File.separator + ".mjolnirr");
        command.addParameter("--index-page");
        command.addParameter("main");
        command.addParameter("--proxy-web-host");
        command.addParameter("localhost");
        command.addParameter("--proxy-web-port");
        command.addParameter("8090");
        command.addParameter("--proxy-queue-port");
        command.addParameter("5445");
        command.addParameter("--config");
        command.addParameter(project.getBasePath() + File.separator + ".mjolnirr-config");
        command.addParameter("--workspace");
        command.addParameter(project.getBasePath() + File.separator + ".mjolnirr-workspace");

        String configName = config.getName();
        String title = "Run '" + configName + "'";

        processHandler = new OSProcessHandler(command.createProcess(), title);
        processHandler.putUserData(MJOLNIRR_RUN_CONFIGURATION_KEY, config);

        sendInfoToLaunchedInstance(project, config.getContainerPort(), config.getModuleComponentName());
    }

    private void sendInfoToLaunchedInstance(final Project project, final int port, final String appName) {
        new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(BROWSER_RUNNING_TIMEOUT * 1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }

                try {
                    BrowserRunner.run("hive://localhost:" + port + "/" + appName);
                } catch (Exception e) {
                    StatusBar statusBar = WindowManager.getInstance().getStatusBar(project);

                    JBPopupFactory.getInstance()
                            .createHtmlTextBalloonBuilder(String.format("<strong>%s</strong> <br/> %s", "Browser instance not found", "Have you started Mjolnirr browser?"), MessageType.ERROR, null)
                            .setFadeoutTime(7500)
                            .createBalloon()
                            .show(RelativePoint.getCenterOf(statusBar.getComponent()), Balloon.Position.atRight);
                }
            }
        }.start();
    }

    public boolean getIsAppAlreadyRunning() {
        return appAlreadyRunning;
    }

    public OSProcessHandler getProcessHandler() {
        return processHandler;
    }

    public enum BuildMode {
        BUILD_ONLY,
        BUILD_AND_RUN,
        BUILD_RUN_LOG,
        LOG_ONLY
    }
}
