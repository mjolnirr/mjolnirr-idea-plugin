package com.mjolnirr.idea.build;

import com.intellij.openapi.vfs.VirtualFile;
import com.mjolnirr.idea.MjolnirrApplicationComponent;

import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 07.11.13
 * Time: 14:35
 * To change this template use File | Settings | File Templates.
 */
public class MjolnirrListCellRenderer extends DefaultListCellRenderer {
    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        if (value != null) {
            VirtualFile f = (VirtualFile) value;
            setText(f.getName());
            setIcon(MjolnirrApplicationComponent.ICON);
        } else {
            setText("none");
            setIcon(null);
        }
        return this;
    }
}
