package com.mjolnirr.idea.build;

import com.intellij.execution.configurations.ConfigurationFactory;
import com.intellij.execution.configurations.ConfigurationType;
import com.intellij.execution.configurations.RunConfiguration;
import com.intellij.openapi.project.Project;
import com.mjolnirr.idea.MjolnirrApplicationComponent;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 05.11.13
 * Time: 19:48
 * To change this template use File | Settings | File Templates.
 */
public class MjolnirrConfigFactory extends ConfigurationFactory {
    protected MjolnirrConfigFactory(@NotNull ConfigurationType type) {
        super(type);
    }

    @Override
    public Icon getAddIcon() {
        return MjolnirrApplicationComponent.ADD_ICON;
    }

    @Override
    public RunConfiguration createTemplateConfiguration(Project project) {
        return new MjolnirrRunConfiguration(project, this);
    }
}
