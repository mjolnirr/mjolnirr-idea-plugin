package com.mjolnirr.idea.build;

import com.intellij.execution.ExecutionException;
import com.intellij.execution.Executor;
import com.intellij.execution.configurations.*;
import com.intellij.execution.runners.ExecutionEnvironment;
import com.intellij.execution.runners.ProgramRunner;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.options.SettingsEditor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ProjectRootManager;
import com.intellij.openapi.util.DefaultJDOMExternalizer;
import com.intellij.openapi.util.InvalidDataException;
import com.intellij.openapi.util.JDOMExternalizable;
import com.intellij.openapi.util.WriteExternalException;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.JavaPsiFacade;
import com.intellij.psi.PsiClass;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.search.searches.ClassInheritorsSearch;
import com.mjolnirr.idea.MjolnirrApplicationComponent;
import com.mjolnirr.idea.manifest.Util;
import org.jdom.Element;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 05.11.13
 * Time: 19:53
 * To change this template use File | Settings | File Templates.
 */
public class MjolnirrRunConfiguration extends ModuleBasedConfiguration {
    private static final String MODULE_KEY = "module";
    private static final String CONTAINER_PORT_KEY = "container-port";
    private static final String CONTAINER_LOCATION_KEY = "container-location";
    private static final int CONTAINER_DEFAULT_PORT = 8090;

    private VirtualFile selectedModule = null;
    private int containerPort = CONTAINER_DEFAULT_PORT;
    private String containerJARLocation;

    public boolean UseV1Format = false;
    public boolean UseSeparateOutputFolders = true;
    public boolean FollowLogs = true;
    public int V8DebugPort = 5858;

    public MjolnirrRunConfiguration(Project project, MjolnirrConfigFactory factory) {
        super(project.getName(), new RunConfigurationModule(project), factory);
    }

    public List<VirtualFile> getProjectModules() {
        return new ArrayList(Arrays.asList(ProjectRootManager.getInstance(getProject()).getContentRootsFromAllModules()));
    }

    public VirtualFile getSelectedModule() {
        return selectedModule;
    }

    public void setSelectedModule(VirtualFile selectedModule) {
        this.selectedModule = selectedModule;
    }

    public String getModuleComponentName() {
        return Util.getFirstMjolnirrComponent(getProject(), selectedModule);
    }

    public String getContainerJARLocation() {
        return containerJARLocation;
    }

    public void setContainerJARLocation(String containerJARLocation) {
        this.containerJARLocation = containerJARLocation;
    }

    public int getContainerPort() {
        return containerPort;
    }

    public void setContainerPort(int containerPort) {
        this.containerPort = containerPort;
    }

    @Nullable
    public String getOutputFolder() {
        if(!UseSeparateOutputFolders)
            return null;
        try {
            return getProject().getBaseDir().getPath() + "/bin/" + getName();
        } catch(NullPointerException e) {
            return null;
        }
    }

    @Override
    public Collection<Module> getValidModules() {
        return getAllModules();
    }

    @Override
    protected ModuleBasedConfiguration createInstance() {
        return new MjolnirrRunConfiguration(getProject(), (MjolnirrConfigFactory) getFactory());
    }

    public MjolnirrRunConfigurationEditor getConfigurationEditor() {
        return new MjolnirrRunConfigurationEditor();
    }

    public RunProfileState getState(@NotNull Executor executor, @NotNull ExecutionEnvironment executionEnvironment) throws ExecutionException {
        return new MjolnirrRunningState(executor, executionEnvironment);
    }

    private VirtualFile readFileSetting(Element element, String entry) {
        String path = element.getChildText(entry);
        if (path != null)
            return LocalFileSystem.getInstance().findFileByPath(path);
        return null;
    }

    @Override
    public void readExternal(Element element) throws InvalidDataException {
        super.readExternal(element);
        DefaultJDOMExternalizer.readExternal(this, element);

        selectedModule = readFileSetting(element, MODULE_KEY);
        String containerPortString = element.getChildText(CONTAINER_PORT_KEY);
        containerJARLocation = element.getChildText(CONTAINER_LOCATION_KEY);
        containerPort = Integer.parseInt((containerPortString == null) ? "" + CONTAINER_DEFAULT_PORT : containerPortString);
    }

    private void writeFileSetting(Element element, String entry, VirtualFile file) {
        if (file != null)
            element.addContent(new Element(entry).setText(file.getPath()));
    }

    @Override
    public void writeExternal(Element element) throws WriteExternalException {
        super.writeExternal(element);
        DefaultJDOMExternalizer.writeExternal(this, element);

        writeFileSetting(element, MODULE_KEY, selectedModule);
        element.addContent(new Element(CONTAINER_PORT_KEY).setText("" + containerPort));
        element.addContent(new Element(CONTAINER_LOCATION_KEY).setText(containerJARLocation));
    }

    @Override
    public String suggestedName() {
        return super.suggestedName();
    }
}
