package com.mjolnirr.idea.build;

import com.intellij.execution.ExecutionException;
import com.intellij.execution.ExecutionManager;
import com.intellij.execution.Executor;
import com.intellij.execution.configurations.CommandLineState;
import com.intellij.execution.filters.TextConsoleBuilder;
import com.intellij.execution.filters.TextConsoleBuilderFactory;
import com.intellij.execution.process.OSProcessHandler;
import com.intellij.execution.process.ProcessAdapter;
import com.intellij.execution.process.ProcessEvent;
import com.intellij.execution.runners.ExecutionEnvironment;
import com.intellij.execution.ui.RunContentDescriptor;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.project.Project;
import com.intellij.ui.content.Content;
import com.intellij.ui.content.ContentManager;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 06.11.13
 * Time: 11:08
 * To change this template use File | Settings | File Templates.
 */
public class MjolnirrRunningState extends CommandLineState {
    private ExecutionEnvironment executionEnvironment;
    private Executor executor;

    public MjolnirrRunningState(Executor executor, ExecutionEnvironment executionEnvironment) {
        super(executionEnvironment);
        this.executor = executor;
        this.executionEnvironment = executionEnvironment;
        Project project = executionEnvironment.getProject();
        TextConsoleBuilder builder = TextConsoleBuilderFactory.getInstance().createBuilder(project);
        builder.setViewer(false);
        this.setConsoleBuilder(builder);
    }

    @Override
    protected OSProcessHandler startProcess() throws ExecutionException {
        MjolnirrRunConfiguration configuration = (MjolnirrRunConfiguration) getRunnerSettings().getRunProfile();
        MjolnirrRunner.BuildMode buildMode = configuration.FollowLogs
                ? MjolnirrRunner.BuildMode.BUILD_RUN_LOG
                : MjolnirrRunner.BuildMode.BUILD_AND_RUN;
        MjolnirrRunner runner = new MjolnirrRunner(executionEnvironment.getProject(),
                configuration, buildMode);

        OSProcessHandler processHandler = runner.getProcessHandler();
        final boolean finalAppAlreadyRunning = runner.getIsAppAlreadyRunning();

        processHandler.addProcessListener(new ProcessAdapter() {
            @Override
            public void processTerminated(ProcessEvent event) {
                if (finalAppAlreadyRunning) {
                    RunContentDescriptor contentDescriptor = ExecutionManager.getInstance(executionEnvironment.getProject()).
                            getContentManager().findContentDescriptor(executor, event.getProcessHandler());
                    if (contentDescriptor != null) {
                        final Content content = contentDescriptor.getAttachedContent();
                        ApplicationManager.getApplication().invokeLater(new Runnable() {
                            public void run() {
                                String oldName = content.getDisplayName();
                                ContentManager manager = content.getManager();

                                manager.removeContent(content, true);
                                Content oldContent = manager.findContent(oldName);
                                if (oldContent != null)
                                    manager.setSelectedContent(oldContent);
                            }
                        });
                    }
                }
            }
        });
        return processHandler;
    }

    public OSProcessHandler createProcessWithoutLogging() throws ExecutionException {
        MjolnirrRunner runner = new MjolnirrRunner(executionEnvironment.getProject(),
                (MjolnirrRunConfiguration) getRunnerSettings().getRunProfile(),
                MjolnirrRunner.BuildMode.BUILD_AND_RUN);

        return runner.getProcessHandler();
    }

    public OSProcessHandler createLogOnlyProcess() throws  ExecutionException {
        MjolnirrRunner runner = new MjolnirrRunner(executionEnvironment.getProject(),
                (MjolnirrRunConfiguration) getRunnerSettings().getRunProfile(),
                MjolnirrRunner.BuildMode.LOG_ONLY);

        return runner.getProcessHandler();
    }
}
