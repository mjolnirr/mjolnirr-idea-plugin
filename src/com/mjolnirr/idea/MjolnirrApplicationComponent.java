package com.mjolnirr.idea;

import com.intellij.ide.plugins.cl.PluginClassLoader;
import com.intellij.openapi.components.ApplicationComponent;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ProjectRootManager;
import com.intellij.openapi.util.SystemInfo;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiDirectory;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: sk_
 * Date: 05.11.13
 * Time: 18:45
 * To change this template use File | Settings | File Templates.
 */
public class MjolnirrApplicationComponent implements ApplicationComponent {
    public static final Icon ICON = new ImageIcon(MjolnirrApplicationComponent.class.getResource("icons/icon.png"));
    public static final Icon ADD_ICON = new ImageIcon(MjolnirrApplicationComponent.class.getResource("icons/icon.png"));
    public static final Icon NATURE_ICON = new ImageIcon(MjolnirrApplicationComponent.class.getResource("icons/icon.png"));

    public static final String PLUGIN_ID = "com.mjolnirr.idea";
    public static final String COMPONENT_NAME = "Mjolnirr development plugin";

    public MjolnirrApplicationComponent() {
    }

    public void initComponent() {
//        ClassLoader classLoader = this.getClass().getClassLoader();
//
//        if(classLoader instanceof PluginClassLoader) {
//            try {
//                PluginClassLoader pluginClassLoader = (PluginClassLoader)classLoader;
//                File webOSToolsJarFile = new File(MjolnirrApplicationComponent.getWebOSToolsJarPath());
//                URL webOSToolsJarURL = webOSToolsJarFile.toURI().toURL();
//                pluginClassLoader.addURL(webOSToolsJarURL);
//            } catch (MalformedURLException e) {
//                //  Show notification
//            }
//        }
    }

    public void disposeComponent() {
        // TODO: insert component disposal logic here
    }

    @NotNull
    public String getComponentName() {
        return COMPONENT_NAME;
    }

    public static List<VirtualFile> getWebOSAppFoldersInProject(Project project) {
        return collectDirsInProject(project, new IsAppDir());
    }

    public static List<VirtualFile> getWebOSPackageFoldersInProject(Project project) {
        return collectDirsInProject(project, new IsPackageDir());
    }

    public static List<VirtualFile> getWebOSServiceFoldersInProject(Project project) {
        return collectDirsInProject(project, new IsServiceDir());
    }

    public static List<VirtualFile> getWebOSAccountsFoldersInProject(Project project) {
        return collectDirsInProject(project, new IsAccountsDir());
    }

    private static List<VirtualFile> collectDirsInProject(Project project, CheckDirectory checker) {
        ArrayList<VirtualFile> files = new ArrayList<VirtualFile>();
        VirtualFile baseDir = project.getBaseDir();
        if (baseDir != null) {
            if (checker.checkDir(baseDir))
                files.add(baseDir);
            VirtualFile[] children = baseDir.getChildren();
            for (VirtualFile child : children) {
                if (child.isDirectory()) {
                    if (checker.checkDir(child)) {
                        files.add(child);
                    }
                }
            }
        }
        return files;
    }

    public static VirtualFile getAppDirForFile(VirtualFile file) {
        while (file != null) {
            if (MjolnirrApplicationComponent.isWebOSAppDir(file)) {
                return file;
            }
            file = file.getParent();
        }
        return null;
    }

    public static boolean isWebOSAppDir(PsiDirectory dir) {
        return isWebOSAppDir(dir.getVirtualFile());
    }

    public static boolean isWebOSAppDir(VirtualFile file) {
        if(!file.isDirectory())
            return false;
        boolean hasAppInfo = file.findChild("appinfo.json") != null;
        boolean parentIsResources = false;
        VirtualFile parent = file.getParent();
        if(parent != null)
            parentIsResources = parent.getName().equals("resources");
        return hasAppInfo && !parentIsResources;
    }

    public static boolean isWebOSPackageDir(VirtualFile file) {
        return file.isDirectory() && (file.findChild("packageinfo.json") != null);
    }

    public static boolean isWebOSServiceDir(VirtualFile file) {
        return file.isDirectory() && (file.findChild("services.json") != null);
    }

    public static boolean isWebOSAccountsDir(VirtualFile file) {
        return file.isDirectory() && (file.findChild("account-templates.json") != null);
    }

    public static String getPalmSDKPath() {
        if(SystemInfo.isWindows)
            return System.getenv("PalmSDK");
        else// if(SystemInfo.isMac || SystemInfo.isLinux)
            return "/opt/PalmSDK/Current";
    }

    public static String getWebOSToolsJarPath() {
        return getPalmSDKPath()+"/share/jars/webos-tools.jar";
    }

    private abstract static class CheckDirectory {
        public abstract boolean checkDir(VirtualFile dir);
    }

    private static class IsAppDir extends CheckDirectory {
        @Override
        public boolean checkDir(VirtualFile dir) {
            return MjolnirrApplicationComponent.isWebOSAppDir(dir);
        }
    }

    private static class IsPackageDir extends CheckDirectory {
        @Override
        public boolean checkDir(VirtualFile dir) {
            return MjolnirrApplicationComponent.isWebOSPackageDir(dir);
        }
    }

    private static class IsServiceDir extends CheckDirectory {
        @Override
        public boolean checkDir(VirtualFile dir) {
            return MjolnirrApplicationComponent.isWebOSServiceDir(dir);
        }
    }

    private static class IsAccountsDir extends CheckDirectory {
        @Override
        public boolean checkDir(VirtualFile dir) {
            return MjolnirrApplicationComponent.isWebOSAccountsDir(dir);
        }
    }
}
